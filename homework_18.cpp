﻿#include <iostream>


using namespace std;

template <typename T>

class Stack
{
public:

    void push(T const value)
    {
        if (top < size)
        {
            array[top] = value;
            top++;
        }
        else
        {
            T* newArray = new T[size * 2];
            for (int i = 0; i < size; i++)
            {
                newArray[i] = array[i];
            }
            newArray[top] = value;
            delete[] array;
            array = newArray;
            size *= 2;
            top++;
        }
        
    }
    
  /*  void print()
    {  

        for (int i = 0; i < size; i++)
        {
            cout << array[i] << '\t';
        }
        cout << '\n';
    }*/

    bool pop(T * item)
    {
        bool res = false;
        if (top > 0)
        {
            *item = array[--top];
            res = true;
        };
        return res;
    }

    ~Stack()
    {
        delete[] array;
    }

private:
    
    int top = 0;
    int size = 4;
    T* array = new T[size];
     

};

int main()
{

    Stack<char> t;
    for (int i = 'a'; i <= 'z'; i++)
    {
        t.push(i);
    }
    char item;
    while (t.pop(&item))
    {
        cout << "item= " << item << endl;
    };
    return 0;
}
